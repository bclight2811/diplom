"use strict";
module.exports = class Mongo {
    constructor(model) {
        this.model = model;
    }

    getList({query, select, sort, limit}) {
        return new Promise((resolve, reject) => {
            this.model
                .find(query)
                .select(select)
                .sort(sort)
                .limit(Number(limit))
                .exec(function (err, list) {
                    if (err) {
                        reject(err);
                    }
                    resolve(list);
                });
        });
    }

    updateDocuments({query, update, options}) {
        return new Promise((resolve, reject) => {
            this.model.update(query, update, options, function (err, res) {
                if (err) {
                    reject(err);
                }
                resolve(res);
            })
        })
    }

    saveDocuments(body) {
        return new Promise((resolve, reject) => {
            this.model.create(body, function (err, document) {
                if (err) {
                    reject(err);
                }
                resolve(document);
            })
        })
    }

    deleteDocuments(body) {
        return new Promise((resolve, reject) => {
            this.model.remove(body, function (err, document) {
                if (err) {
                    reject(err);
                }
                resolve(document);
            })
        })
    }
};