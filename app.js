"use strict";
const express      = require('express'),
      path         = require('path'),
      cookieParser = require('cookie-parser'),
      session      = require('express-session'),
      bodyParser   = require('body-parser'),
      config       = require('config'),
      compression  = require('compression'),
      logger       = require('log4js').getLogger(),
      MongoStore   = require('connect-mongo')(session),
      mongoose     = require('libs/mongoose'),
      favicon      = require('serve-favicon');

//require('./createCooks');
const routIndex = require('./routes/index'),
      api       = require('./routes/api');

const app    = express(),
      server = require('http').Server(app),
      io     = require('socket.io')(server);
server.listen(config.get('port'));

//socket
io.on('connection', function (socket) {

    socket.on('test', function (data) {
        socket.broadcast.emit('busy', data)
    });

    socket.on('new-order', function (data) {
        socket.emit('success-done-order', data);
        io.emit('new-order-on-kitchen', data)
    });

    socket.on('start_cook_product', function (data) {
        io.emit('start_cook_product', data)
    });

    socket.on('order_on_table', function (data) {
        io.emit('order_on_table', data)
    });

});

// view engine setup
app.set('views', path.join(__dirname, '/template'));
app.set('view engine', 'pug');

app.use(favicon(__dirname + '/public/favicon.ico'));
//body Parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
//cookie-session
app.use(cookieParser());
app.use(session({
    secret:            config.get('session:secret'),
    resave:            false,
    saveUninitialized: true,
    key:               config.get('session:key'),
    cookie:            config.get('session:cookie'),
    store:             new MongoStore({mongooseConnection: mongoose.connection})
}));
app.use(compression());
//static
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api', api);
app.use('/', routIndex);

app.use(function (req, res, next) {
    let err    = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error   = req.app.get('env') === 'development' ? err : {};
    logger.error(err.message);
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

app.set('port', process.env.PORT || 3000);
logger.info('Server running on port: ' + config.get('port'));
module.exports = app;

