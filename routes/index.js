const express = require('express'),
      config  = require('config'),
      router  = express.Router();

router.use(function (req, res, next) {
    res.render('index', {port: config.get('port'), address: config.get('address')});
});

module.exports = router;
