"use strict";
const express    = require('express'),
      router     = express.Router(),
      crypto     = require('crypto'),
      User       = require('models/users').User,
      Menu       = require('models/menu').Menu,
      Session    = require('models/session').Session,
      Table      = require('models/table').Table,
      Order      = require('models/orders').Order,
      ClassMongo = require('Class/Mongo'),
      drone      = require('netology-fake-drone-api');

const modules = {
    user:  User,
    menu:  Menu,
    order: Order,
    table: Table
};

let getUsers = new Promise((resolve, reject) => {
    User.find({}, function (err, users) {
        resolve(users);
    })
});

router.post('/v1/get/session', function (req, res, next) {
    let sess = req.session;
    if (!sess.restaurant || !sess.restaurant.userType || sess.restaurant.userType == 0) {
        res.status(401).end('not found session');
    } else {

        req.session.reload(function (err) {
            res.json(req[req.body.session]);
        });
    }
});

router.post('/v1/set/session', function (req, res, next) {
    if (req.session.restaurant) {
        req.session.restaurant = req.body;
        req.session.reload(function (err) {
            if (err) {
                next(err);
            }
            req.session.restaurant = req.body;
            res.json(req.session.restaurant);
        });
    } else {
        req.session.restaurant = req.body;
        req.session.save(function (err) {
            res.json(req.session.restaurant);
        });
    }
});

router.post('/v1/get/:model', function (req, res) {
    let body = req.body;
    let menu = new ClassMongo(modules[req.params.model]);
    menu.getList(body)
        .then(list => res.json(list));
});

router.post('/v1/update/:model', function (req, res) {
    let body = req.body;
    let menu = new ClassMongo(modules[req.params.model]);
    menu.updateDocuments(body)
        .then(list => res.json(list));
});

router.post('/v1/save/:model', function (req, res) {
    let body = req.body;
    let menu = new ClassMongo(modules[req.params.model]);
    menu.saveDocuments(body)
        .then(list => res.json(list));
});

router.post('/v1/delete/:model', function (req, res) {
    let body = req.body;
    let menu = new ClassMongo(modules[req.params.model]);
    menu.deleteDocuments(body)
        .then(list => res.json(list));
});

router.post('/v1/authorize/user', function (req, res) {
    let username = req.body.username;
    let password = req.body.password;
    User.authorize(username, password, function (err, user) {
        if (err) {
            res.status(401).end('not found User');
        }
        res.json(user);
    })
});

router.post('/v1/deliver/', function (req, res) {
    drone
        .deliver()
        .then(function () {res.end('1')})
        .catch(function () {res.end('0')});
});

module.exports = router;
