"use strict";
const mongoose = require('libs/mongoose'),
      Schema   = mongoose.Schema,
      schema   = new Schema({
          number: {
              type: Number,
              index: true
          },
          isBusy:Boolean,
          userId:String
      });

exports.Table = mongoose.model('Table', schema, 'tables');