const mongoose = require('libs/mongoose'),
      Schema   = mongoose.Schema,
      schema   = new Schema();

exports.Session = mongoose.model('Session', schema);