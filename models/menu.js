const mongoose = require('libs/mongoose'),
      Schema   = mongoose.Schema,
      schema   = new Schema({
          title: {
              type: String,
              required: true
          },
          price: {
              type: Number,
              required: true
          },
          rating:Number,
          id:Number,
          ingredients: {
              type: [],
              required: true
          },
          image:String
      });

exports.Menu = mongoose.model('Menu', schema, 'menu');