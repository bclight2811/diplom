"use strict";
const mongoose = require('libs/mongoose'),
      Schema   = mongoose.Schema,
      schema   = new Schema({
          numberTable:    Number,
          products:       [],
          userName:       String,
          price:          Number,
          userId:         String,
          isOnTable:      {
              type:    Boolean,
              default: false
          },
          isPay:          {
              type:    Boolean,
              default: false
          },
          isErrorDeliver: {
              type:    Boolean,
              default: false
          }
      });

exports.Order = mongoose.model('Order', schema, 'orders');