"use strict";
const gulp   = require('gulp'),
      pug    = require('gulp-pug'),
      concat = require('gulp-concat');

gulp.task('libs-js', () => {
    gulp.src([
        'node_modules/angular/angular.min.js',
        'node_modules/angular-route/angular-route.min.js',
        'node_modules/angular-animate/angular-animate.min.js',
        'node_modules/angular-aria/angular-aria.min.js',
        'node_modules/angular-messages/angular-messages.min.js',
        'node_modules/angular-material/angular-material.min.js',
        'node_modules/angular-material-expansion-panel/dist/md-expansion-panel.min.js',
        'node_modules/angular-socket-io/socket.js'
    ])
        .pipe(gulp.dest('public/libs/js'))
        .pipe(concat('libs.js'))
        .pipe(gulp.dest('public/libs/js'));
    gulp.src('node_modules/socket.io-client/dist/socket.io.js')
        .pipe(gulp.dest('public/libs/js'));
});
gulp.task('libs-css', () => {
    gulp.src([
        'node_modules/angular-material/angular-material.min.css',
        'node_modules/angular-material-expansion-panel/dist/md-expansion-panel.min.css'
    ])
        .pipe(concat('libs.css'))
        .pipe(gulp.dest('public/libs/css'))
});
gulp.task('pug', () => {
    gulp.src([
        'public/controller/**/*.pug'
    ])
        .pipe(pug())
        .pipe(gulp.dest('public/controller'))
});

gulp.task('build', ['libs-js', 'libs-css', 'pug']);