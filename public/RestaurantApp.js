"use strict";
const restaurantApp = angular.module('RestaurantApp', ['ngMaterial', 'ngRoute', 'ngMessages', 'btford.socket-io', 'material.components.expansionPanels']);

angular
    .module('RestaurantApp')
    .config(['$routeProvider', '$locationProvider',
        function ($routeProvider, $locationProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'controller/MainPage/MainPage.html',
                    controller: 'MainPageCtrl'
                })
                .when('/kitchen', {
                    templateUrl: 'controller/KitchenPage/KitchenPage.html',
                    controller: 'KitchenPageCtrl'
                })
                .when('/login', {
                    templateUrl: 'controller/RestaurantEnter/RestaurantEnter.html',
                    controller: 'RestaurantEnterCtrl'
                })
                .otherwise({
                    redirectTo: '',
                });
            $locationProvider.html5Mode(true);
        }
    ])
    .run(['$location', 'Mongo', '$window', function ($location, Mongo, $window) {
        Mongo.getSession({session: 'session'})
             .then(function (session) {
                 if (getDataSession(session).userType == 2) {
                     $location.path('/kitchen');
                 } else {
                     $location.path('/');
                 }
             })
             .catch(function (session) {
                 $location.path('login');
             })
    }])
    .factory('Socket', function (socketFactory) {
        const socket = io.connect();
        return socketFactory({
            ioSocket: socket
        });
    })
    .controller('HeadCtrl', function ($scope, $location) {
        $scope.title = 'Лучший ресторан в Москве';
    });

const getDataSession = session => session.data.restaurant;