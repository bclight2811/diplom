'use strict';
restaurantApp
    .controller('KitchenPageCtrl', function ($scope, Mongo, Socket, $q, $timeout, $interval, Api) {
        $scope.successGetOrders = true;
        $scope.orders           = [];

        $scope.plate = {
            first:  {
                busy:    false,
                value:   0,
                order:   '',
                product: '',
                time:    0
            },
            second: {
                busy:    false,
                value:   0,
                order:   '',
                product: '',
                time:    0
            }
        };

        Socket.on('new-order-on-kitchen', function (data) {
            $scope.orders.push(data.data);
        });

        $q
            .all([Mongo.getOrder({query: {isOnTable: false}})])
            .then(function (res) {
                $scope.successGetOrders = false;
                $scope.orders           = res[0].data;
            });

        $scope.isReady    = product => product.isReady == 1;
        $scope.isReadyAll = order => order.products.find(product => product.isReady == 0) == undefined;

        $scope.getProductAndOrderId = function (plateNumber) {
            let idOrder   = $scope.orders.findIndex(order => order._id == $scope.plate[plateNumber].order);
            let idProduct = $scope.orders[idOrder].products.findIndex(product => product._id == $scope.plate[plateNumber].product);
            return {idOrder, idProduct};
        };

        $scope.startCook = function (time, plateNumber) {
            $scope.plate[plateNumber].busy = true;
            let {idOrder, idProduct}       = $scope.getProductAndOrderId(plateNumber);
            Socket.emit('start_cook_product', {
                orderID:   $scope.orders[idOrder]._id,
                productId: $scope.orders[idOrder].products[idProduct]._id
            });
            $scope.cooking = function (callback) {
                $scope.cookingTimer = $interval(function () {
                    $scope.plate[plateNumber].value++;
                    callback($scope.plate[plateNumber].value);
                }, 1000 * (time / 100 ), 100);
            };

            $scope.cooking(function (value) {
                if (value >= 100) {
                    let {idOrder, idProduct}       = $scope.getProductAndOrderId(plateNumber);
                    $scope
                        .orders[idOrder]
                        .products[idProduct]
                        .isReady = 1;
                    Mongo
                        .updateOrder({
                            query:  {
                                _id:            $scope.orders[idOrder]._id,
                                'products._id': $scope.orders[idOrder].products[idProduct]._id
                            },
                            update: {'products.$.isReady': 1}
                        });

                    $scope.plate[plateNumber].value   = 0;
                    $scope.plate[plateNumber].busy    = false;
                    $scope.plate[plateNumber].order   = '';
                    $scope.plate[plateNumber].product = '';
                    $scope.plate[plateNumber].time    = 0;
                }
            });

        };

        $scope.sendToCustomer = function (order) {
            let indexOrder = $scope.orders.findIndex(item => order._id == item._id);
            $scope.orders.splice(indexOrder, 1);
            Api
                .deliver()
                .then(function (res) {
                    if (res.data == '1') {
                        Mongo
                            .updateOrder({
                                query:  {_id: order._id},
                                update: {isOnTable: true}
                            });
                        Socket.emit('order_on_table', {orderID: order._id, success: true});
                    } else if (res.data == '0') {
                        Mongo
                            .updateOrder({
                                query:  {_id: order._id},
                                update: {isErrorDeliver: true}
                            });
                        Socket.emit('order_on_table', {orderID: order._id, success: false});
                    }
                });
        };

        $scope.isOnPlate = function (order, product) {
            return ($scope.plate.first.order == order._id &&
                $scope.plate.first.product == product._id) ||
                ($scope.plate.second.order == order._id &&
                $scope.plate.second.product == product._id) ||
                ($scope.plate.first.busy && $scope.plate.second.busy);

        };

        $scope.onPlate = function (order, product) {
            if (!$scope.plate.first.busy) {
                $scope.plate.first.busy    = true;
                $scope.plate.first.order   = order._id;
                $scope.plate.first.product = product._id;
                $scope.plate.first.time    = product.price * 2;
                $scope.startCook($scope.plate.first.time, 'first');
            } else if (!$scope.plate.second.busy) {
                $scope.plate.second.busy    = true;
                $scope.plate.second.order   = order._id;
                $scope.plate.second.product = product._id;
                $scope.plate.second.time    = product.price * 2;
                $scope.startCook($scope.plate.second.time, 'second');
            }
        };

        $scope.refresh = function (data) {
            $scope.orders.push(data);
            $scope.successGetOrders = true;
            $timeout(function () {
                $scope.successGetOrders = false;
            }, 50);
        };

        $scope.timeCook = function (time) {
            $scope.second = time % 60;
            $scope.minute = (time - $scope.second) / 60;
            return `${$scope.minute >= 10 ? $scope.minute : '0' + $scope.minute}:${$scope.second >= 10 ? Math.round($scope.second) : '0' + Math.round($scope.second)}`;
        };
    });
