'use strict';

restaurantApp
    .controller('RestaurantEnterCtrl', ['$scope', 'Mongo', '$location', function ($scope, Mongo, $location) {
        $scope.minPay = 0;
        Mongo
            .getMenu({sort: 'price', select: 'price', limit: 1})
            .then(res => $scope.minPay = res.data[0].price);

        $scope.submitKitchen = function () {
            Mongo
                .authorizeUser({username: $scope.kitchen.login, password: $scope.kitchen.password})
                .then(function () {Mongo.setSession({username: $scope.kitchen.login, userType: 2})})
                .then($location.path('/kitchen'))
                .catch(res => console.log('Неверный логин или пароль'));
        };

        $scope.submitGuest = function () {
            Mongo
                .setSession({name: $scope.guest.name, pay: $scope.guest.pay, userType: 1})
                .then($location.path('/'));
        }

    }]);
