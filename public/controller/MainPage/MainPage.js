'use strict';
restaurantApp
    .controller('MainPageCtrl', function ($scope, Mongo, Socket, $q, $mdDialog, $interval, $rootScope, $timeout) {
        $scope.loadPage      = false;
        $scope.socket        = Socket;
        $scope.uTable        = 0;
        $scope.tables        = [];
        $scope.timeOutWaiter = 30;
        $rootScope.user      = {};
        $scope.timer         = [];
        $scope.loading       = {
            successGetTable:   false,
            successUseTable:   false,
            successCallWaiter: false,
            successDoneOrder:  false
        };

        Socket.on('busy', function (data) {
            $scope.tables[(+data - 1)].userId = 'busy';
            $scope.tables[(+data - 1)].isBusy = true;
        });

        $q
            .all([Mongo.getTable({}), Mongo.getSession({session: 'sessionID'}), Mongo.getSession({session: 'session'})])
            .then(function (res) {
                $scope.tables = res[0].data;
                for (let table of $scope.tables) {
                    if (table.userId == res[1].data) {
                        $scope.$broadcast('user', res[2].data.restaurant);
                        $scope.uTable     = table.number;
                        $rootScope.uTable = $scope.uTable;
                        $scope.waiterTimeInterval(30, 'waiter');
                    }
                }
                $rootScope.user   = res[2].data.restaurant;
                $rootScope.userId = res[1].data;
                $scope.loading.successGetTable = true;
            });

        $scope.showModalMenu = function () {
            $interval.cancel($scope.waitWaiterTimer);
            $mdDialog
                .show({
                    controller:          DialogWithWaiter,
                    parent:              angular.element(document.body),
                    clickOutsideToClose: true,
                    templateUrl:         'dialogContent.tmpl.html'
                })
        };

        $scope.intervalOfTimeWithCallback = function (variableName, time, functionName) {
            $scope.waitWaiter = function (callback) {
                $scope.waitWaiterTimer = $interval(function () {
                    $scope.timer[variableName]--;
                    callback($scope.timer[variableName]);
                }, 1000, time)
            };

            $scope.waitWaiter(function (variableName) {
                if (variableName == 0) {
                    functionName();
                }
            });
        };

        $scope.waiterTimeInterval = function (time, name) {
            $scope.timer[name] = time;
            $scope.intervalOfTimeWithCallback(name, time, $scope.showModalMenu);
            return $scope.timer[name];
        };

        $scope.takeTable = function (number) {

            $q
                .all([Mongo.getSession({session: 'sessionID'}), Mongo.getSession({session: 'session'})])
                .then(function (res) {
                    Mongo.updateTables({query: {number: number}, update: {isBusy: true, userId: res[0].data}})
                         .then(function () {
                             $scope.tables[number - 1].userId = res[0].data;
                             Socket.emit('test', number);
                         });
                });
            $scope.uTable                    = number;
            $rootScope.uTable                = $scope.uTable;
            $scope.loading.successUseTable   = true;
            $scope.loading.successCallWaiter = true;
            $scope.waiterTimeInterval(30, 'waiter');
        };

        $scope.youTable = table => table.number == $scope.uTable;

        $scope.isNotEmptyUserId = table => table.userId !== '';

        $scope.youTableClass = function (table) {
            if ($scope.youTable(table)) {
                return 'you-table';
            } else if ($scope.isNotEmptyUserId(table)) {
                return 'other-table';
            }
        };

        $scope.isDisabledTable = table => $scope.uTable > 0 ? true : table.isBusy;

        $scope.isBusyMessage = function (table) {
            if ($scope.isDisabledTable(table)) {
                if ($scope.youTable(table)) {
                    return 'Ваш столик';
                } else if ($scope.isNotEmptyUserId(table)) {
                    return 'Столик занят';
                }
                return `Столик № ${table.number}`;
            } else {
                return `Выбрать столик № ${table.number}`;
            }
        };
    });
