/**
 * Created by Евгений on 19.02.2017.
 */
"use strict";
function DialogWithWaiter($scope, $mdDialog, Mongo, $rootScope, $timeout, Socket, $q, $interval) {

    $scope.hide     = () => $mdDialog.hide();
    $scope.cancel   = () => $mdDialog.cancel();
    $scope.answer   = answer => $mdDialog.hide(answer);
    $scope.callback = callback => $mdDialog.hide(callback());

    $scope.user   = $rootScope.user;
    $scope.userId = $rootScope.userId;

    $scope.loading          = false;
    $scope.successDoneOrder = false;
    $scope.notEmoughtMoney  = false;

    $scope.isCooking   = (order, product) => product.isReady == 0 && product.startCook == true && order.isOnTable == false;
    $scope.isReady     = (order, product) => product.isReady == 1 && order.isOnTable == false;
    $scope.isOnTable   = order => order.isOnTable == true && order.isErrorDeliver == false;
    $scope.isOnKitchen = (order, product) => product.isReady == 0 && product.startCook == false && order.isOnTable == false;
    $scope.menu        = [];
    $scope.orders      = [];
    $scope.selected    = [];

    $scope.selectedIndex       = 0;
    $scope.orderPrice          = 0;
    $scope.temporaryOrderPrice = 0;

    $scope.timerOnKitchen = function () {
        let self = this;
        self.timerFunc = $interval(function () {
            self.timer++;
        }, 1000);
        return self;
    };

    $scope.startCookTimer = function (indexOrder, indexProduct) {
        let time       = $scope.orders[indexOrder].products[indexProduct].price * 2;
        $scope.cooking = function (callback) {
            $scope.cookingTimer = $interval(function () {
                $scope.orders[indexOrder].products[indexProduct].timerCook++;
                callback($scope.orders[indexOrder].products[indexProduct].timerCook);
            }, 1000 * (time / 100 ), 100);
        };
        $scope.cooking(function (value) {
            if (value >= 100) {
                $scope
                    .orders[indexOrder]
                    .products[indexProduct]
                    .isReady = 1;
            }
        })
    };

    Socket.on('start_cook_product', function (data) {
        let indexOrder   = $scope.orders.findIndex(order => order._id == data.orderID);
        let indexProduct = $scope.orders[indexOrder].products.findIndex(product => product._id == data.productId);
        $scope
            .orders[indexOrder]
            .products[indexProduct]
            .startCook   = true;
        $scope.startCookTimer(indexOrder, indexProduct);
    });

    Socket.on('order_on_table', function (data) {
        let indexOrder = $scope.orders.findIndex(order => order._id == data.orderID);
        if (data.success) {
            $scope.orders[indexOrder].isOnTable = true;
            $interval.cancel($scope.orders[indexOrder].timerFunc);
        } else {
            $scope.orders[indexOrder].isErrorDeliver = true;
        }
    });

    $q
        .all([
            Mongo.getMenu({}),
            Mongo.getOrder({query: {userId: $scope.user.userId, isPay: false}})
        ])
        .then(function (res) {
            $scope.loading  = true;
            $scope.menu     = res[0].data;
            $scope.orders   = res[1].data.reduce(function (mass, item) {
                item = $scope.timerOnKitchen.apply(item);
                item.timer    = 0;
                item.products = item.products.map(function (product) {
                    product.startCook = false;
                    product.timerCook = 0;
                    return product;
                });
                mass.push(item);
                return mass;
            }, []);

            $scope.selected = res[1].data.reduce(function (mass, item) {
                for (let product of item.products) {
                    mass.push(product);
                }
                return mass;
            }, []);
            if ($scope.selected.length) {
                $scope.initializationOrder();
            }
        });

    $scope.getOrderPrice = () => $scope.temporaryOrderPrice = $scope.selected.reduce((sum, index) => sum += index.price, 0);

    $scope.toggle = function (item, list) {
        let idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        }
        else {
            list.push(item);
        }
        $scope.getOrderPrice();
    };

    $scope.clearSelect = function () {
        $scope.selected            = [];
        $scope.temporaryOrderPrice = 0;
    };

    $scope.initializationOrder = function () {
        $scope.getOrderPrice();
        $scope.orderPrice += $scope.temporaryOrderPrice;
        $scope.successDoneOrder = true;
        $scope.selectedIndex    = 1;
        $scope.clearSelect();
    };

    $scope.timeCook = function (time, order) {
        if(order.isOnTable) return 'Готово';
        let second = time % 60;
        let minute = (time - second) / 60;
        return `${minute >= 10 ? minute : '0' + minute}:${second >= 10 ? Math.round(second) : '0' + second}`;
    };

    $scope.setOrder = function (selected) {
        if ($scope.temporaryOrderPrice <= $scope.user.pay) {
            $scope.user.pay -= $scope.temporaryOrderPrice;
            selected = $scope.selected.map(function (item) {
                item.isReady = 0;
                item.time    = item.price * 2;
                return item;
            });
            $q
                .all([
                    Mongo.setOrder({
                        numberTable: $rootScope.uTable,
                        products:    selected,
                        userName:    $scope.user.name,
                        price:       $scope.temporaryOrderPrice,
                        userId:      $scope.user.userId
                    }),
                    Mongo.setSession($scope.user)
                ])
                .then(function (res) {
                    res[0].data.timer = 0;
                    $scope.orders.push(res[0].data);
                    $scope.orders.products = res[0].data.products.map(function (product) {
                        product.startCook = false;
                        product.timerCook = 0;
                        return product;
                    });
                    $scope.initializationOrder();
                    let indexOrder = $scope.orders.findIndex(item => item._id == res[0].data._id);
                    $scope.orders[indexOrder] = $scope.timerOnKitchen.apply($scope.orders[indexOrder]);
                    Socket.emit('new-order', res[0]);
                })
        } else {
            $scope.replenishBalance = $scope.temporaryOrderPrice - $scope.user.pay;
            $scope.notEmoughtMoney  = true;
            $timeout(function () {
                $scope.notEmoughtMoney = false;
            }, 5000)
        }
    };

    $scope.deleteIndexOrder = function (order) {
        let indexOrder = $scope.orders.findIndex(item => item._id == order._id);
        $scope.orders.splice(indexOrder, 1);
    };
    $scope.returnMoney      = function (order) {
        $scope.user.pay += order.price;
        $scope.deleteIndexOrder(order);
        Mongo.setSession($scope.user);
        Mongo.deleteOrder({_id: order._id});
    };

    $scope.payBySale = function (order) {
        let indexOrder                           = $scope.orders.findIndex(item => item._id == order._id);
        $scope.orders[indexOrder].isErrorDeliver = false;
        $scope.orders[indexOrder].products       = $scope.orders[indexOrder].products.map(function (item) {
            item.price   = item.price * 0.95;
            item.isReady = 0;
            return item;
        });
        Mongo.updateOrder({query: {_id: order._id}, update: {products: $scope.orders[indexOrder].products, isErrorDeliver: false}});
        let data = {data: $scope.orders[indexOrder]};
        Socket.emit('new-order', data);
    };

    $scope.payOrder = function (order) {
        $scope.deleteIndexOrder(order);
        Mongo.deleteOrder({_id: order._id});
    };

    $scope.exists = (item, list) => list.indexOf(item) > -1;
}

