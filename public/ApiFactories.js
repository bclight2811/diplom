/**
 * Created by Евгений on 12.02.2017.
 */

const postRestWithPromise = (address, body, $http) => {
    return new Promise((resolve, reject) =>
        $http.post(address, body)
             .then(function successCallback(response) {
                 resolve(response);
             }, function errorCallback(response) {
                 reject(response);
             }));
};

angular
    .module('RestaurantApp')
    .factory('Api', $http => ({
        deliver: body => postRestWithPromise('../api/v1/deliver/', body, $http)
    }))
    .factory('Mongo', $http => ({
        getMenu: body => postRestWithPromise('../api/v1/get/menu/', body, $http),
        authorizeUser: body => postRestWithPromise('../api/v1/authorize/user/', body, $http),
        getTable: body => postRestWithPromise('../api/v1/get/table/', body, $http),
        getOrder: body => postRestWithPromise('../api/v1/get/order/', body, $http),
        setOrder: body => postRestWithPromise('../api/v1/save/order/', body, $http),
        updateOrder: body => postRestWithPromise('../api/v1/update/order/', body, $http),
        deleteOrder: body => postRestWithPromise('../api/v1/delete/order/', body, $http),
        updateTables: body => postRestWithPromise('../api/v1/update/table/', body, $http),
        getSession: body => postRestWithPromise('../api/v1/get/session/', body, $http),
        setSession: body => postRestWithPromise('../api/v1/set/session/', body, $http)
    }));